import csv

import Code.loader as loader
import numpy as np
import pandas as pd

selected = np.load("DatasetPrepare/selectedvendors.npy")
print selected

treshold = 5

testVendors = selected[:treshold]
trainVendors = selected[treshold:]

files = loader.getOnlyFiles("embeddings")


def loadVendor(name):
    result = []
    for x in files:
        if x.find(name) >= 0:
            result.append(x)
    return result


from shutil import copyfile

def prepareSmallDataset(vendors):
    for v in vendors:
        fnames = loadVendor(v)
        for f in fnames:
            data = pd.read_csv(f)
            mistakes = data[(data["normalized"] == '-11111') & (data["class"] == 1)]
            if(len(mistakes)>0):
                assert len(mistakes) == 1
                print mistakes
                if mistakes["word"].values[0][0] == '#':
                    index = mistakes.index[0]
                    print index
                    assert data["class"].values[index] == 1
                    assert data["class"].values[index + 1] == 2
                    data["class"].values[index] = 0
                    data["class"].values[index + 1] = 1
                    data.to_csv(f, index=False, quoting=csv.QUOTE_NONNUMERIC)

smallTest = ["d1f", "d2f", "d3f"]

prepareSmallDataset(smallTest)

def copyVendors(vendors, path):
    for v in vendors:
        fnames = loadVendor(v)
        for f in fnames:
            copyfile(f, path + f.split("\\")[-1])



print testVendors
print trainVendors

copyVendors(smallTest, "dataset/testSmall/")
copyVendors(smallTest, "dataset/testAll/")
copyVendors(testVendors, "dataset/testAll/")
copyVendors(trainVendors, "dataset/train/")

testVendors = np.load("DatasetPrepare/test.npy")
trainVendors = np.load("DatasetPrepare/train.npy")


print testVendors
print trainVendors

files = loader.getOnlyFiles(u"C:/diploma/convertedDataset")
copyVendors(testVendors, "dataset/test3/")
copyVendors(trainVendors, "dataset/train3/")


