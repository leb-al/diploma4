import theano.tensor as T
import numpy as np
from lasagne import init
from lasagne import nonlinearities
from lasagne.layers import DenseLayer


class SoftmaxDenseLayer(DenseLayer):
    def __init__(self, incoming, num_units, W=init.GlorotUniform(), b=init.Constant(0.), num_leading_axes=1, **kwargs):
        super(SoftmaxDenseLayer, self).__init__(incoming, num_units, W, b, nonlinearities.softmax, num_leading_axes, **kwargs)
        if num_leading_axes != 2:
            raise ValueError("Work only with 3D tensor and 2 leading axes")
        if len(self.input_shape) != num_leading_axes + 1:
            raise ValueError('Wrong num leading axis')

    def get_output_for(self, input, **kwargs):
        flatten = T.reshape(input, (-1, T.shape(input)[-1]))

        activation = T.dot(flatten, self.W)
        if self.b is not None:
            activation = activation + self.b
        result = nonlinearities.softmax(activation)

        shape = T.shape(input)

        #shffledinput = input.dimshuffle(2, 0, 1)
        #shape = T.shape(T.tensordot(shffledinput[0], np.zeros(self.num_units), 0))

        #shape = T.tensor_copy(T.shape(input))
        #shape[2] = self.num_units
        return T.reshape(result, (shape[0], shape[1], self.num_units), ndim=3)
