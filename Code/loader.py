from __future__ import print_function
from os import listdir
from os.path import isfile, join

import numpy as np
import pandas as pd

from Code.features import DataEncoder
from Code.trainer import TestDataSet
from Code.logger import FileLogger


def getOnlyFiles(dirpath):
    return [join(dirpath, f) for f in listdir(dirpath) if isfile(join(dirpath, f)) & (f != 'thumbs.db')]


def loadf(fname):
    return pd.read_csv(fname)


def getDataSet(dirpath):
    onlyfiles = getOnlyFiles(dirpath)
    return [loadf(x) for x in onlyfiles]


def splitTrainVal(trainData):
    np.random.shuffle(trainData)
    count = int(0.8 * len(trainData))
    validationData = trainData[count:]
    trainData = trainData[0:count]
    return trainData, validationData


def numClasses():
    return [5, 14]


class LegacyDataLoader:
    def __init__(self, use_grams, use_embeddings):
        self.encoder = DataEncoder(use_grams, use_embeddings)
        assert not use_grams
        assert not use_embeddings

    def convert(self, trainData, validationData, testData):
        self.encoder.preprocessData(trainData)
        self.encoder.preprocessData(validationData)
        self.encoder.preprocessData(testData)

        valX = self.encoder.convertX(validationData)
        valY = self.encoder.convertY(validationData)
        trainX = self.encoder.convertX(trainData)
        trainY = self.encoder.convertY(trainData)
        testX = self.encoder.convertX(testData)
        testY = self.encoder.convertY(testData)
        return trainX, trainY, valX, valY, testX, testY

    def saveFilesNames(self, dirpath):
        onlyfiles = getOnlyFiles(dirpath)
        outfile = open('filenames.txt', "w")
        for f in onlyfiles:
            print(f, file=outfile)

    def loadStandart(self, trainds, testds, logger):
        self.saveFilesNames(testds)
        testData = getDataSet(testds)
        trainData = getDataSet(trainds)
        trainData, validationData = splitTrainVal(trainData)
        logger.printline("Data loaded")
        return self.convert(trainData, validationData, testData)

    def featuresCount(self):
        return self.encoder.featureCount()

    def numClasses(self):
        return numClasses()


class StandartDataLoader:
    def __init__(self, use_grams, use_embeddings, use_stadard_feachers=True, use_spellchecker=False):
        self.encoder = DataEncoder(use_grams, use_embeddings, use_stadard_feachers, use_spellchecker)

    def convert(self, trainData, validationData, testDataSets, testDirectories):
        self.encoder.preprocessData(trainData)

        valX = self.encoder.convertX(validationData)
        valY = self.encoder.convertY(validationData)
        trainX = self.encoder.convertX(trainData)
        trainY = self.encoder.convertY(trainData)

        tests = []

        assert len(testDataSets) == len(testDirectories)

        for i in range(len(testDataSets)):
            testX = self.encoder.convertX(testDataSets[i])
            testY = self.encoder.convertY(testDataSets[i])
            prefix = testDirectories[i]
            tests.append(TestDataSet(testX, testY, prefix, FileLogger(prefix, False)))

        return trainX, trainY, valX, valY, tests

    def saveFilesNames(self, dirpath, filename):
        onlyfiles = getOnlyFiles(dirpath)
        outfile = open('trainRes/' + filename + '_filenames.txt', "w")
        for f in onlyfiles:
            print(f, file=outfile)

    def loadStandart(self, trainds, testDirectories, logger):
        testDataSets = []
        for dirn in testDirectories:
            path = 'dataset/' + dirn
            self.saveFilesNames(path, dirn)
            testDataSets.append(getDataSet(path))
        trainData = getDataSet(trainds)
        trainData, validationData = splitTrainVal(trainData)
        logger.printline("Data loaded")
        return self.convert(trainData, validationData, testDataSets, testDirectories)

    def featuresCount(self):
        return self.encoder.featureCount()

    def numClasses(self):
        return numClasses()


class VendorsLoader:
    def __init__(self):
        self.encoder = DataEncoder(False, False)

    def convertTrainValidation(self, trainData, validationData):
        valX = self.encoder.convertX(validationData)
        valY = self.encoder.convertY(validationData)
        trainX = self.encoder.convertX(trainData)
        trainY = self.encoder.convertY(trainData)
        return trainX, trainY, valX, valY

    def convertTest(self, testData):
        testX = self.encoder.convertX(testData)
        testY = self.encoder.convertY(testData)
        return testX, testY

    def loadVendors(self, dirpath, vendors):
        data = []
        for v in vendors:
            vendordata = self.loadVendor(dirpath, v)
            for f in vendordata:
                data.append(f)
        trainData, validationData = splitTrainVal(data)
        print("Data loaded")
        return self.convertTrainValidation(trainData, validationData)

    def getVendors(self, dirpath):
        onlyfiles = getOnlyFiles(dirpath)
        prefix = dirpath + '\\bdADDRESS.'
        vendors = set()
        for x in onlyfiles:
            assert x.startswith(prefix)
            fname = x[len(prefix):]
            tokens = fname.split(".")
            assert len(tokens) == 3
            vendors.add(tokens[0])

        vendorsarr = []
        print("Found {} vendors:".format(len(vendors)))
        for v in vendors:
            print(v)
            vendorsarr.append(v)

        np.sort(vendorsarr)
        return vendorsarr

    def loadVendor(self, dirpath, name):
        files = getOnlyFiles(dirpath)
        result = []
        for x in files:
            if x.find(name) > 0:
                result.append(loadf(x))
        return result

    def featuresCount(self):
        return self.encoder.featureCount()

    def numClasses(self):
        return numClasses()
