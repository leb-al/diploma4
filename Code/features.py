# coding=utf-8
from __future__ import print_function

import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer

from Code.spelling import SpellChecker

# Датасеты географии
db1 = pd.read_csv('uszipsv1.1.csv')
db2 = pd.read_csv('zip_code_database.csv')
states = {st for st in db2['state'].unique()}
cities = {city.upper() for city in np.unique(np.concatenate([db1['city'].unique(), db2['primary_city'].unique()]))}
zips = {zcode for zcode in np.unique(np.concatenate([db1[db1.columns[0]].unique(), db2[db2.columns[0]].unique()]))}
citiesSpeller = SpellChecker(cities)


def getFeatureEncoders():
    featuresEncoders = []
    featuresEncoders.append(
        lambda x: len(x)
    )
    featuresEncoders.append(
        lambda x: x.isdigit()
    )
    featuresEncoders.append(
        lambda x: x.strip('#').isdigit()
    )
    featuresEncoders.append(
        lambda x: x.strip(',').isdigit()
    )
    featuresEncoders.append(
        lambda x: x.isupper()
    )
    featuresEncoders.append(
        lambda x: x[0].isupper() if len(x) > 0 else 0
    )

    def checkUpperTrimmed(x):
        y = x.strip(',')
        return y[0].isupper() if len(y) > 0 else 0

    def checkState(x):
        y = x.strip(',').upper()
        # y = x.strip(',')
        return y in states

    def checkStateSharp(x):
        y = x.strip('#').strip(',').upper()
        return y in states

    def checkCity(x):
        y = x.strip(',').upper()
        # y = x.strip(',')
        return y in cities

    def checkZip(x):
        y = x.strip(',')
        if y.isdigit():
            return int(y) in zips
        else:
            return False

    featuresEncoders.append(
        lambda x: x.find('$') >= 0
    )
    featuresEncoders.append(
        lambda x: ((len(x) == 5) & x.isdigit())
    )
    featuresEncoders.append(
        lambda x: len(x) > 6
    )
    featuresEncoders.append(
        lambda x: x.find(',') >= 0
    )
    featuresEncoders.append(
        lambda x: x.find('.') >= 0
    )
    featuresEncoders.append(
        lambda x: x.find('-') >= 0
    )
    featuresEncoders.append(
        lambda x: x.find('+') >= 0
    )
    featuresEncoders.append(
        lambda x: x.find('*') >= 0
    )
    featuresEncoders.append(
        lambda x: x.find('!') >= 0
    )
    featuresEncoders.append(
        lambda x: x.find('#') >= 0
    )
    featuresEncoders.append(
        lambda x: x.find('~') >= 0
    )
    featuresEncoders.append(
        lambda x: x.find(')') >= 0
    )
    featuresEncoders.append(
        lambda x: x.find('(') >= 0
    )
    featuresEncoders.append(
        lambda x: np.sum([int(x[i].isdigit()) for i in range(len(x))])
    )
    featuresEncoders.append(
        lambda x: x.upper() == "STREET" or x.upper().strip(".") == "STR" or x.upper().strip(".") == "SR"
    )
    featuresEncoders.append(
        lambda x: x.upper() == "ROAD" or x.upper() == "ROUTE" or x.upper() == "WAY" or x.upper().strip(".") == "RD"
    )
    featuresEncoders.append(
        lambda x: x.upper() == "AVENUE" or x.upper().strip(".") == "AVE"
    )
    featuresEncoders.append(
        lambda x: x.upper() == "CITY" or x.upper() == "TOWN"
    )
    featuresEncoders.append(
        lambda x: x.upper() == "CENTER" or x.upper() == "MAIN"
    )
    featuresEncoders.append(
        lambda x: x.upper() == "STORE" or x.upper() == "STORE" or x.upper() == "MARKET"
    )
    featuresEncoders.append(
        lambda x: x.upper() == "VALLEY" or x.upper() == "LAKE" or x.upper() == "RIVER"
    )
    featuresEncoders.append(
        lambda x: x.upper().strip(".") == "N" or x.upper().strip(".") == "W" or
                  x.upper().strip(".") == "S" or x.upper().strip(".") == "E" or x.upper().strip(".") == "NORTH" or
                  x.upper().strip(".") == "NORTH" or x.upper().strip(".") == "EAST" or x.upper().strip(".") == "WEST"
    )
    featuresEncoders.append(checkUpperTrimmed)
    featuresEncoders.append(checkState)
    featuresEncoders.append(checkStateSharp)
    featuresEncoders.append(checkCity)
    featuresEncoders.append(checkZip)
    return featuresEncoders


max_distance = 2

levenshteinDict = {}


def getSpellCheckingFeatureEndocers():
    def prepareCity(x):
        return x.strip(',').upper()

    def levenshteinFeature(y):
        if y in cities:
            return max_distance
        else:
            for i in range(max_distance):
                if len(citiesSpeller.search(y, i)) > 0:
                    return max_distance + 1 - i
        return 0

    def memoizeLevenshteinFeature(x):
        y = prepareCity(x)
        result = levenshteinDict.get(y, None)
        if result is None:
            result = levenshteinFeature(y)
            levenshteinDict[y] = result
        return result

    featuresEncoders = []
    featuresEncoders.append(memoizeLevenshteinFeature)
    return featuresEncoders


def getNormalizedEncoders():
    featuresEncoders = []
    dict = ["PHONE", "PHONE-", "1-11", "86459 1-11", "-", "1", "11", "11-11", "F", "1111", "TOTAL", "YOU", "111", "SAVINGS", "FOR", "11-11-11", "111111", "-1-11", "I", "-11", "TO", "YOUR", "T", "SC",
            "TAX", "B", "A", "OF", "--", "----", "AT", "CHANGE", "CARD", "STORE", "ITEMS", "1111111111", "PRICE", "11111", "SAVED", "OR", "ON", "THE", "AND", "POINTS", "PURCHASE", "CASH", "-11-11",
            "-111-", "111111111111", "SUBTOTAL"]

    for word in dict:
        featuresEncoders.append(lambda x: x == word)

    return featuresEncoders


def convertNormalizedWordForNgram(x):
    if x == np.nan:
        return "_NAN_"
    return '_' + str(x).upper() + '_'


class DataEncoder:
    def __init__(self, use_grams, use_embedding, use_standard_feachers, use_dictionaries):
        if use_standard_feachers:
            self.featuresEncoders = getFeatureEncoders()
            self.normalizedEncoders = getNormalizedEncoders()
        else:
            self.featuresEncoders = []
            self.normalizedEncoders = []
        if use_dictionaries:
            dictionaryEncoder = getSpellCheckingFeatureEndocers()
            for e in dictionaryEncoder:
                self.featuresEncoders.append(e)
        self.use_grams = use_grams
        self.use_embedding = use_embedding
        if use_embedding:
            from Code.glove_pretrained import representation, vect_dimetions
            self.embedding_coder = representation
            self.embedding_dim = vect_dimetions()
        else:
            self.embedding_dim = 0
        if use_grams:
            self.ngram_vectorizer = CountVectorizer(input="content", analyzer="word", tokenizer=None, preprocessor=convertNormalizedWordForNgram, ngram_range=(2, 3), min_df=0.01, max_df=0.25)

    def preprocessData(self, arr):
        if self.use_grams:
            data = []
            for df in arr:
                file = []
                for x in df["normalized"].values:
                    file.append(str(x).upper())
                data.append("_" + "_".join(file) + "_")
            self.ngram_vectorizer.fit(data)

    def createXforFile(self, elem):
        features = np.array([[ftr(str(word)) for ftr in self.featuresEncoders] for word in elem["word"].values])
        normalized_features = np.array([[ftr(str(word).upper()) for ftr in self.normalizedEncoders] for word in elem["normalized"].values])
        lineinfo = np.array(elem["lineNumber"]).astype(np.float)
        diffline = lineinfo - np.concatenate(([0], lineinfo[:-1]))
        lineinfo /= np.max(lineinfo)
        linefeatures = np.array(elem[["lineStart", "lineEnd"]])
        res = np.concatenate((features, normalized_features, lineinfo.reshape((-1, 1)), diffline.reshape((-1, 1)), linefeatures), axis=1)
        if self.use_embedding:
            res = np.concatenate((res, [self.embedding_coder(word) for word in elem["normalized"].values]), axis=1)
        if self.use_grams:
            input = [str(x).upper() for x in elem["normalized"].values]
            transform = self.ngram_vectorizer.transform(input).todense()
            res = np.concatenate((res, transform), axis=1)
        return res

    def convertX(self, arr):
        return [self.createXforFile(elem) for elem in arr]

    def convertY(self, arr):
        if "componentClass" in arr[0].columns:
            return [[np.array(elem["class"]) for elem in arr], [np.array(elem["componentClass"]) for elem in arr]]
        else:
            return [[np.array(elem["class"]) for elem in arr]]

    def featureCount(self):
        ngrams_count = 0
        if self.use_grams:
            ngrams_count = np.shape(self.ngram_vectorizer.transform(["_A_"]))[1]
        return len(self.featuresEncoders) + 4 + len(self.normalizedEncoders) + self.embedding_dim + ngrams_count
