from __future__ import print_function

import lasagne
import numpy as np
import sklearn.metrics as skm
import theano
import theano.tensor as T

import Code.timers as timers


def precision_(tp, fp):
    return tp / (tp + fp)


def recall_(tp, fn):
    return tp / (tp + fn)


def fmetric(precision, recall):
    return 2 * precision * recall / (precision + recall)


def fmetricarr(arr):
    assert len(arr) == 3
    return fmetric(precision_(arr[0], arr[1]), recall_(arr[0], arr[2]))


def totalf(fs):
    return fmetricarr(np.sum(fs, axis=1))


def allfs(fs):
    return [fmetricarr(x) for x in np.transpose(fs)]


def needDebug(epoch):
    return epoch % 10 == 0


def assertDoubleEq(a, b):
    if (np.isnan(a) or np.isnan(b)):
        return
    try:
        assert (abs(a - b) <= (a * 1E-7))
        assert (abs(a - b) <= (b * 1E-7))

    except AssertionError:
        print(a, b)
        raise


def createBatch(indexes, dataX, dataY, featuresCount, trainersCount):
    maxl = np.max([dataX[i].shape[0] for i in indexes])
    batchSize = np.size(indexes)
    batchX = np.zeros((batchSize, maxl, featuresCount), np.float32)
    batchY = np.zeros((trainersCount, batchSize, maxl), np.int32)
    batchMask = np.zeros((batchSize, maxl), np.float32)
    j = 0
    for i in indexes:
        curLgth = dataX[i].shape[0]
        np.copyto(batchX[j, 0:curLgth, 0:featuresCount], dataX[i])
        np.copyto(batchMask[j, 0:curLgth], np.ones(curLgth, np.int32))
        for k in range(trainersCount):
            np.copyto(batchY[k, j, 0:curLgth], dataY[k][i])
        j = j + 1
    return batchX, batchY, batchMask


class OneNetTrainer:
    def __init__(self, generator, num_classes, val_printer, learning_rate=0.1, rate_scale=0.9):
        self.l_in, self.l_mask, self.l_out, loss, self.predict = generator
        self.target_values = T.imatrix('target_output')
        self.mask_values = self.l_mask.input_var
        self.cost = loss(lasagne.layers.get_output(self.l_out, deterministic=False), self.target_values, self.mask_values).sum()
        self.num_classes = num_classes

        self.learning_rate = learning_rate
        self.rate_scale = rate_scale

        self.accuracy = T.sum(T.eq(self.target_values, self.predict) * self.mask_values)
        self.fs2 = [self.fmetricForClass(x) for x in range(self.num_classes)]
        self.fs1, _ = theano.scan(lambda x: self.fmetricForClass(x), sequences=T.arange(num_classes))

        self.all_params = lasagne.layers.get_all_params(self.l_out)
        self.compute_cost = theano.function([self.l_in.input_var, self.target_values, self.mask_values], self.cost)
        self.compute_eqcount = theano.function([self.l_in.input_var, self.target_values, self.mask_values], self.accuracy)
        self.compute_predict = theano.function([self.l_in.input_var, self.mask_values], self.predict)
        self.compute_fs = theano.function([self.l_in.input_var, self.target_values, self.mask_values], self.fs1)
        self.compute_fmetric = theano.function([self.l_in.input_var, self.target_values, self.mask_values], self.fmetric_all(T.arange(num_classes)))

        def compute_metrics():
            classes = T.arange(num_classes)
            tps, _ = theano.scan(lambda x: self.gettp(x), sequences=classes)
            fps, _ = theano.scan(lambda x: self.getfp(x), sequences=classes)
            fns, _ = theano.scan(lambda x: self.getfn(x), sequences=classes)
            compute_tps = theano.function([self.l_in.input_var, self.target_values, self.mask_values], tps)
            compute_fps = theano.function([self.l_in.input_var, self.target_values, self.mask_values], fps)
            compute_fns = theano.function([self.l_in.input_var, self.target_values, self.mask_values], fns)
            return [compute_tps, compute_fps, compute_fns]

        self.compiled_metrics = compute_metrics()

        self.testAccurs = []
        self.testF = []
        self.testFs = []

        self.valAccurs = []
        self.valF = []
        self.valFs = []

        self.valLog = val_printer

        self.testDatasets = None

        print("Worker created")

    def metrics_as_array(self, xbch, ybch, mbch):
        return np.array([f(xbch, ybch, mbch) for f in self.compiled_metrics])

    def metrics_for_f(self, classNum):
        resultIn = self.predict[(T.eq(self.target_values, classNum) * self.mask_values).nonzero()]
        resultOut = self.predict[(T.neq(self.target_values, classNum) * self.mask_values).nonzero()]
        tp = T.eq(resultIn, classNum).sum()
        fp = T.eq(resultOut, classNum).sum()
        tn = T.neq(resultOut, classNum).sum()
        fn = T.neq(resultIn, classNum).sum()
        return tp, fp, tn, fn

    def precision(self, classNum):
        tp, fp, _, _ = self.metrics_for_f(classNum)
        return precision_(tp, fp)

    def recall(self, classNum):
        tp, _, _, fn = self.metrics_for_f(classNum)
        return recall_(tp, fn)

    def fmetricForClass(self, classnum):
        return fmetric(self.precision(classnum), self.recall(classnum))

    def gettp(self, classnum):
        tp, _, _, _ = self.metrics_for_f(classnum)
        return tp

    def getfp(self, classnum):
        _, fp, _, _ = self.metrics_for_f(classnum)
        return fp

    def getfn(self, classnum):
        _, _, _, fn = self.metrics_for_f(classnum)
        return fn

    def fmetric_all(self, classes):
        tps, _ = theano.scan(lambda x: self.gettp(x), sequences=classes)
        fps, _ = theano.scan(lambda x: self.getfp(x), sequences=classes)
        fns, _ = theano.scan(lambda x: self.getfn(x), sequences=classes)
        tps = tps.sum()
        fps = fps.sum()
        fns = fns.sum()
        return fmetric(precision_(tps, fps), recall_(tps, fns))

    def printVal(self, line):
        self.valLog.printline(line)

    def genTrainFunc(self, epoch):
        updates = lasagne.updates.adam(self.cost, learning_rate=self.learning_rate * (self.rate_scale ** epoch), params=self.all_params)
        return theano.function([self.l_in.input_var, self.target_values, self.mask_values], self.cost, updates=updates)

    def getTestAccuracy(self):
        return self.testAccurs

    def getTestF(self):
        return self.testF

    def getTestFs(self):
        return self.testFs


class Trainer:
    def __init__(self, trainers, features_count):
        self.trainers = trainers
        self.features_count = features_count
        self.timer = timers.Timer()

        self.trainX = None
        self.trainY = None
        self.valX = None
        self.valY = None
        self.testDatasets = None

        print("All workers created")

    def setData(self, trainX, trainY, validationX, validationY, test_datasets):
        self.trainX = trainX
        self.trainY = trainY
        self.valX = validationX
        self.valY = validationY
        self.testDatasets = test_datasets

    def testEpoch(self, epoch, batch_size, dataset, trainersIndex):
        def receiptNums(receipt):
            return ''.join([str(num) + ',' for num in receipt])

        accur_test_d = 0
        trainerscount = len(self.trainers)

        outfile = open('trainRes/' + str(dataset.prefix) + 'trainer' + str(trainersIndex) + 'epoch' + str(epoch) + ".txt", "w")
        indx = np.arange(len(dataset.testX))
        batchCount = (len(indx) + batch_size - 1) // batch_size
        fs = np.zeros((3, self.trainers[trainersIndex].num_classes))
        resulty = np.zeros(0)
        groundtrouth = np.zeros(0)
        elemcount = 0

        for i in range(batchCount):
            startIndx = i * batch_size
            endIndx = startIndx + batch_size
            X, y, m = createBatch(indx[startIndx:endIndx], dataset.testX, dataset.testY, self.features_count, trainersIndex + 1)
            batchPredict = self.trainers[trainersIndex].compute_predict(X, m)

            fs += self.trainers[trainersIndex].metrics_as_array(X, y[trainersIndex], m)
            if needDebug(epoch):
                accur_test_d += self.trainers[trainersIndex].compute_eqcount(X, y[trainersIndex], m)
                elemcount += np.sum(m)

            for j in range(X.shape[0]):
                print('[' + receiptNums(batchPredict[j, :len(dataset.testY[trainersIndex][j + startIndx])]) + ']', file=outfile)
                resulty = np.concatenate((resulty, batchPredict[j, :len(dataset.testY[trainersIndex][j + startIndx])]))
                groundtrouth = np.concatenate((groundtrouth, dataset.testY[trainersIndex][j + startIndx]))

        fmetrics_test = allfs(fs)

        accur_test = skm.accuracy_score(groundtrouth, resulty)
        fmetric_test = skm.f1_score(groundtrouth, resulty, average='macro')

        if needDebug(epoch):
            fmetric_test_d = np.mean(fmetrics_test)
            accur_test_d /= elemcount
            assertDoubleEq(accur_test, accur_test_d)
            assertDoubleEq(fmetric_test, fmetric_test_d)

        self.trainers[trainersIndex].testAccurs.append(accur_test)
        self.trainers[trainersIndex].testF.append(fmetric_test)
        self.trainers[trainersIndex].testFs.append(fmetrics_test)

        dataset.logger.printline("{:3d} | {:01.5f} | {:01.5f} | {}".format(epoch, accur_test, fmetric_test, fmetrics_test))

    def trainEpoch(self, epoch, batch_size):
        epoch_timer = timers.Timer()
        self.timer.add_sub_timer(epoch_timer)
        self.timer.begin_timer("Epoch {:03d}".format(epoch))

        epoch_timer.begin_timer("Train")
        testsize = len(self.trainX)
        trainerscount = len(self.trainers)
        indx = np.arange(testsize)
        np.random.shuffle(indx)
        batchCount = (len(indx) + batch_size - 1) // batch_size
        trainFunc = [trainer.genTrainFunc(epoch) for trainer in self.trainers]
        for i in range(batchCount):
            startIndx = i * batch_size
            endIndx = startIndx + batch_size
            X, y, m = createBatch(indx[startIndx:endIndx], self.trainX, self.trainY, self.features_count, trainerscount)
            for j in range(trainerscount):
                trainFunc[j](X, y[j], m)

        cost_val = np.zeros(trainerscount)
        accur_val = np.zeros(trainerscount)
        elemcount = np.zeros(trainerscount)

        epoch_timer.begin_timer("Validation")
        valsize = len(self.valX)
        indx = np.arange(valsize)
        batchCount = (len(indx) + batch_size - 1) // batch_size
        fs = [np.zeros((3, trainer.num_classes)) for trainer in self.trainers]
        for i in range(batchCount):
            startIndx = i * batch_size
            endIndx = startIndx + batch_size
            X, y, m = createBatch(indx[startIndx:endIndx], self.valX, self.valY, self.features_count, trainerscount)
            for j in range(trainerscount):
                trainer = self.trainers[j]
                cost_val[j] += trainer.compute_cost(X, y[j], m)
                accur_val[j] += trainer.compute_eqcount(X, y[j], m)
                fs[j] += trainer.metrics_as_array(X, y[j], m)
                elemcount[j] += np.sum(m)

        for j in range(trainerscount):
            fmetrics_val = allfs(fs[j])
            fmetric_val = np.mean(fmetrics_val)
            accur_val[j] /= elemcount[j]
            trainer = self.trainers[j]
            trainer.valAccurs.append(accur_val)
            trainer.valF.append(fmetric_val)
            trainer.valFs.append(fmetrics_val)
            trainer.printVal("{:3d} | {:05.1f} | {:01.5f} | {:01.5f} | {}".format(epoch, cost_val[j], accur_val[j], fmetric_val, fmetrics_val))
            np.savez('trainRes/net{}epoch{}.npz'.format(j, epoch), *lasagne.layers.get_all_param_values(trainer.l_out))

        epoch_timer.begin_timer("Test")
        for i in range(trainerscount):
            for d in self.testDatasets[i]:
                self.testEpoch(epoch, batch_size, d, i)

        print("{:4.1f} | {}".format(np.sum(epoch_timer.get_values(False)), epoch_timer.get_values(True)))

    def train(self, num_epoch, batch_size=50):
        start = len(self.trainers[0].valAccurs) + 1

        try:
            for epoch in range(num_epoch):
                self.trainEpoch(start + epoch, batch_size)

        except KeyboardInterrupt:
            for trainer in self.trainers:
                trainer.printVal("Training process interrupted")
                # for d in trainer.testDatasets:
                #    d.logger.printline("Training process interrupted")

        print("Timers names: {}".format(self.timer.get_sub_timers()[0].get_names(True)))
        print("Epochs ids and times: {}, times {}".format(self.timer.get_names(False), self.timer.get_values(False)))


class TestDataSet:
    def __init__(self, testX, testY, prefix, logger):
        self.testX = testX
        self.testY = testY
        self.prefix = prefix
        self.logger = logger
