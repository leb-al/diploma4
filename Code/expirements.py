from __future__ import print_function

import numpy as np

import Code.builder as builder
import Code.logger as logger
from Code.loader import LegacyDataLoader, VendorsLoader, StandartDataLoader
from Code.trainer import Trainer, TestDataSet, OneNetTrainer

num_classes = 5


def legacyTrainValTest(trainfilename, testfilename):
    assert False
    val_printer = logger.FileLogger("val", True)
    test_printer = logger.FileLogger("test", False)
    loader = LegacyDataLoader(False, False)
    trainX, trainY, valX, valY, testX, testY = loader.loadStandart(trainfilename, testfilename, val_printer)
    val_printer.printline("Data converted")
    num_classes = loader.numClasses()
    features_count = loader.featuresCount()
    worker = Trainer(builder.genBiLSTMCrf(num_classes, features_count, 30), num_classes, features_count, val_printer)
    datasets = [TestDataSet(testX, testY, "test", test_printer)]
    worker.setData(trainX, trainY, valX, valY, datasets)
    val_printer.printline("Starting train")
    worker.train(45, 1000)


def legascyStandardRun():
    legacyTrainValTest('databig', 'datasmall')


def legacyDebugStandardRun():
    legacyTrainValTest('debugtrain', 'datasmall')


def standartRuns(nets, loader, batch_size, train_dir, test_dirs, learning_rate, rate_scale):
    trainers = []
    trainX, trainY, valX, valY, datasets = loader.loadStandart(train_dir, test_dirs, logger.PrintLogger())
    print("Data converted")
    num_classes = loader.numClasses()
    features_count = loader.featuresCount()
    for i in range(len(nets)):
        val_printer = logger.FileLogger("val" + str(i) + "_", True)
        trainers.append(OneNetTrainer(nets[i], num_classes[i], val_printer, learning_rate[i], rate_scale))
    worker = Trainer(trainers, features_count)
    worker.setData(trainX, trainY, valX, valY, [datasets, []])
    print("Starting train")
    worker.train(60, batch_size)
#
# import lasagne
# def standartRuns(nets, loader, batch_size, train_dir, test_dirs, learning_rate, rate_scale):
#     trainers = []
#     trainX, trainY, valX, valY, datasets = loader.loadStandart(train_dir, test_dirs, logger.PrintLogger())
#     print("Data converted")
#     num_classes = loader.numClasses()
#     features_count = loader.featuresCount()
#     for i in range(len(nets)):
#         val_printer = logger.FileLogger("val" + str(i) + "_", True)
#         trainers.append(OneNetTrainer(nets[i], num_classes[i], val_printer, learning_rate, rate_scale))
#     worker = Trainer(trainers, features_count)
#     worker.setData(trainX, trainY, valX, valY, [datasets, []])
#     print("Starting evaluate")
#     f = np.load('trainRes/net0epoch5.npz')
#     param_values = [f['arr_%d' % i] for i in range(len(f.files))]
#     f.close()
#     lasagne.layers.set_all_param_values(trainers[0].l_out, param_values)
#     worker.testEpoch(44, batch_size, datasets[0], 0)


def standartRun(net, loader, batch_size, train_dir, test_dirs, learning_rate, rate_scale):
    standartRuns([net], loader, batch_size, train_dir, test_dirs, learning_rate, rate_scale)


def legacyReleaseRun(net, loader, batch_size, learning_rate, rate_scale):
    standartRun(net, loader, batch_size, "dataset/train", ["testSmall", "testAll", "testData"], learning_rate, rate_scale)


def releaseRun(nets, loader, batch_size, learning_rate, rate_scale):
    standartRuns(nets, loader, batch_size, "dataset/train3", ["testSmall"], learning_rate, rate_scale)


def debugRun(net, loader, batch_size, learning_rate, rate_scale):
    standartRun(net, loader, batch_size, "debugtrain", ["testSmall"], learning_rate, rate_scale)


def oldCrf():
    loader = StandartDataLoader(False, False)
    releaseRun(builder.genBiLSTMCrf(num_classes, loader.featuresCount(), 30), loader, 800, 0.1, 0.9)


def lstmDropoutDenseCrf():
    loader = StandartDataLoader(False, False)
    releaseRun(builder.getBiLSTMDenseDropoutCrf(num_classes, loader.featuresCount(), 70), loader, 400, 0.01, 0.9)


def lstmDropoutDenseCrfDict():
    loader = StandartDataLoader(False, False, True, True)
    releaseRun(builder.getBiLSTMDenseDropoutCrf(num_classes, loader.featuresCount(), 70), loader, 400, 0.01, 0.9)


def lstmDenseCrf(lr):
    loader = StandartDataLoader(False, False)
    releaseRun(builder.getBiLSTMDenseCrf(num_classes, loader.featuresCount(), 50), loader, 100, lr, 0.9)



def expN2():
    loader = StandartDataLoader(False, False, True, False)
    releaseRun([builder.getBiLSTMDenseCrf(num_classes, loader.featuresCount(), 50),
                builder.getBiLSTMDenseCrf(14, loader.featuresCount(), 70)], loader, 200, [0.005, 0.015], 0.9)

def expN3():
    loader = StandartDataLoader(False, False, True, False)
    releaseRun([builder.getBiLSTMDense(num_classes, loader.featuresCount(), 50),
                builder.getBiLSTMDense(14, loader.featuresCount(), 50)], loader, 700, [0.005, 0.003], 0.85)


def expN4():
    loader = StandartDataLoader(False, False, True, False)
    releaseRun([builder.getBiLSTMDenseCrf(5, loader.featuresCount(), 50),
                builder.getBiLSTMDenseCrf(14, loader.featuresCount(), 50)], loader, 200, [0.005, 0.001], 0.9)
			
def expN5():
    loader = StandartDataLoader(False, True, True, True)
    releaseRun([builder.getBiLSTMDenseCrf(5, loader.featuresCount(), 75),
                builder.getBiLSTMDenseCrf(14, loader.featuresCount(), 75)], loader, 500, [0.005, 0.001], 0.9)
def expN6():
    learning_rate = [0.005, 0.001]
    rate_scale = 0.9
    batch_size = 400
    loader = StandartDataLoader(True, False, True, False)
    trainX, trainY, valX, valY, datasets = loader.loadStandart("dataset/train3", ["testSmall"], logger.PrintLogger())
    print("Data converted")
    nets = [builder.getBiLSTMDenseCrf(5, loader.featuresCount(), 150), builder.getBiLSTMDenseCrf(14, loader.featuresCount(), 150)]
    trainers = []
    num_classes = loader.numClasses()
    features_count = loader.featuresCount()
    for i in range(len(nets)):
        val_printer = logger.FileLogger("val" + str(i) + "_", True)
        trainers.append(OneNetTrainer(nets[i], num_classes[i], val_printer, learning_rate[i], rate_scale))
    worker = Trainer(trainers, features_count)
    worker.setData(trainX, trainY, valX, valY, [datasets, []])
    print("Starting train")
    worker.train(45, batch_size)
	
			
def expN7():
    loader = StandartDataLoader(False, False, True, True)
    releaseRun([builder.getBiLSTMDenseCrf(5, loader.featuresCount(), 50),
                builder.getBiLSTMDenseCrf(14, loader.featuresCount(), 50)], loader, 700, [0.005, 0.001], 0.9)

def lstmDenseCrfDict():
    loader = StandartDataLoader(False, False, True, False)
    releaseRun([builder.getBiLSTMDenseCrf(num_classes, loader.featuresCount(), 50),
                builder.getBiLSTMDenseCrf(14, loader.featuresCount(), 70)], loader, 200, [0.005, 0.015], 0.9)


def lstmDropoutDenseCrfGlove():
    loader = StandartDataLoader(False, True)
    releaseRun(builder.getBiLSTMDenseDropoutCrf(num_classes, loader.featuresCount(), 50), loader, 200, 0.005, 0.9)


def lstmDenseCrfGlove():
    loader = StandartDataLoader(False, True)
    releaseRun(builder.getBiLSTMDenseCrf(num_classes, loader.featuresCount(), 70), loader, 300, 0.01, 0.9)


def lstmDenseCrfGloveNoFeachers():
    loader = StandartDataLoader(False, True, False)
    releaseRun(builder.getBiLSTMDenseCrf(num_classes, loader.featuresCount(), 70), loader, 200, 0.005, 0.9)


def ngramsRun(embeddings, feateres, net):
    train_dir = "dataset/train"
    test_dirs = ["testSmall", "testAll", "testData"]
    loader = StandartDataLoader(True, feateres, embeddings)
    val_printer = logger.FileLogger("val", True)
    trainX, trainY, valX, valY, datasets = loader.loadStandart(train_dir, test_dirs, val_printer)
    val_printer.printline("Data converted")
    num_classes = loader.numClasses()
    features_count = loader.featuresCount()
    worker = Trainer(net(num_classes, loader.featuresCount(), 400), num_classes, features_count, val_printer, 0.005, 0.9)
    worker.setData(trainX, trainY, valX, valY, datasets)
    val_printer.printline("Starting train")
    worker.train(45, 50)


def lstmDropoutDenseCrfNgrams():
    ngramsRun(False, True, builder.getBiLSTMDenseDropoutCrf)


def lstmDenseCrfNgrams():
    ngramsRun(False, True, builder.getBiLSTMDenseCrf)


def lstmDenseCrfNgramsNoFeatures():
    ngramsRun(False, False, builder.getBiLSTMDenseCrf)


def lstmDropoutDenseCrfNgramsNoFeatures():
    ngramsRun(False, False, builder.getBiLSTMDropoutDenseCrf)


def businessCard():
    loader = StandartDataLoader(False, False)
    standartRun(builder.getBiLSTMDenseDropoutCrf(num_classes, loader.featuresCount(), 70), loader, 400, "dataset/train", ["testBusinessCard", "testSmall", "testAll"], 0.01, 0.85)


def lstmDense():
    loader = StandartDataLoader(False, False)
    # releaseRun(builder.getBiLSTMDense(num_classes, loader.featuresCount(), 30), loader, 300, 0.05, 0.9)
    releaseRun(builder.getBiLSTMDense(num_classes, loader.featuresCount(), 70), loader, 200, 0.005, 0.9)
    # releaseRun(builder.getBiLSTMFullDense(num_classes, loader.featuresCount(), 5), loader, 300, 0.05, 0.9)


def lstm():
    loader = StandartDataLoader(False, False)
    releaseRun(builder.genOnlyBilstm(num_classes, loader.featuresCount(), 5), loader, 300, 0.001, 0.9)


def clearerDataSet():
    dirpath = "debugtrain"
    dirpath = "databig"
    val_printer = logger.FileLogger("trainEst", True)
    test_printer = logger.FileLogger("resultEst", True)
    loader = VendorsLoader()
    vendors = np.array(loader.getVendors(dirpath))
    cvcount = 5
    vendorscount = len(vendors)
    test_length = vendorscount // cvcount
    accurs = []
    f = []
    fs = np.zeros((0, 5))
    count = 0
    np.save("Logs\\vendors.txt", vendors)
    for i in range(cvcount):
        val_printer.printline("Case {}".format(i))
        train_names = np.concatenate((vendors[:i * test_length], vendors[(i + 1) * test_length:]))
        test_names = vendors[i * test_length: (i + 1) * test_length]
        trainX, trainY, valX, valY = loader.loadVendors(dirpath, train_names)
        val_printer.printline("Converted")
        num_classes = loader.numClasses()
        features_count = loader.featuresCount()
        worker = Trainer(builder.genBiLSTMCrf(num_classes, features_count, 30), num_classes, features_count, val_printer)
        val_printer.printline("Data converted")
        worker.setData(trainX, trainY, valX, valY, [])
        val_printer.printline("Starting train")
        batch_size = 600
        worker.train(15, batch_size)
        val_printer.printline("Calculating tests")
        for v in test_names:
            testX, testY = loader.convertTest(loader.loadVendor(dirpath, v))
            dataset = TestDataSet(testX, testY, "".replace('\'', '_').replace(' ', '_').replace('&', '_'), test_printer)
            worker.setData(None, None, None, None, [dataset])
            test_printer.printline("{:^20}".format(v), end=' ')
            worker.testEpoch(count, batch_size, dataset)
            count = count + 1
        accurs = np.concatenate((accurs, worker.getTestAccuracy()))
        f = np.concatenate((f, worker.getTestF()))
        fs = np.concatenate((fs, worker.getTestFs()))

    np.save("Logs\\accuracy.txt", accurs)
    np.save("Logs\\f.txt", f)
    np.save("Logs\\fs.txt", fs)
