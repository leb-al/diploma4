from timeit import default_timer as timer
import numpy as np


class Timer:
    def __init__(self):
        self.last_time = timer()
        self.last_id = None
        self.metrics = {}
        self.sub_timers = []

    def end_timer(self):
        end = timer()
        duration = end - self.last_time
        if self.last_id is not None:
            self.metrics[self.last_id] = self.metrics.get(self.last_id, 0) + duration
        for tmr in self.sub_timers:
            tmr.end_timer()
        self.last_id = None
        return end

    def begin_timer(self, id):
        begin = self.end_timer()
        self.last_id = id
        self.last_time = begin

    def add_sub_timer(self, tmr):
        self.sub_timers.append(tmr)

    def get_sub_timers(self):
        return self.sub_timers

    def get_names(self, subtimers):
        result = np.array([i for i in self.metrics.keys()])
        if subtimers:
            for tmr in self.sub_timers:
                result = np.append(result, tmr.get_names())
        result.sort()
        return result

    def get_values(self, subtimers):
        self.end_timer()
        names = self.get_names(False)
        result = []
        for name in names:
            result.append(self.metrics[name])
        if subtimers:
            for tmr in self.sub_timers:
                tmrResult = tmr.get_values(subtimers)
                for v in tmrResult:
                    result.append(v)
        return result
