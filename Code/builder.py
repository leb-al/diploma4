import lasagne
import theano.tensor as T

import objectives
from crf import CRFLayer
from Code.softmax_dense_layer import SoftmaxDenseLayer

# All gates have initializers for the input-to-gate and hidden state-to-gate
# weight matrices, the cell-to-gate weight vector, the bias vector, and the nonlinearity.
# The convention is that gates use the standard sigmoid nonlinearity
gate_parameters = lasagne.layers.recurrent.Gate(
    W_in=lasagne.init.GlorotNormal(), W_hid=lasagne.init.GlorotNormal(),
    b=lasagne.init.Constant(0.5), nonlinearity=lasagne.nonlinearities.sigmoid)

cell_parameters = lasagne.layers.recurrent.Gate(
    W_in=lasagne.init.GlorotNormal(), W_hid=lasagne.init.GlorotNormal(),
    W_cell=lasagne.init.GlorotNormal(), b=lasagne.init.Constant(0.5),
    nonlinearity=lasagne.nonlinearities.tanh)


def genBiLSTM(featuresCount, hiddenNum):
    l_in = lasagne.layers.InputLayer(shape=(None, None, featuresCount))
    l_mask = lasagne.layers.InputLayer(shape=(None, None))
    l_lstm = lasagne.layers.recurrent.LSTMLayer(
        l_in, hiddenNum,
        # We need to specify a separate input for masks
        mask_input=l_mask,
        # Here, we supply the gate parameters for each gate
        ingate=gate_parameters, forgetgate=gate_parameters,
        cell=cell_parameters, outgate=gate_parameters, peepholes=False,
        # We'll learn the initialization and use gradient clipping
        learn_init=True, grad_clipping=100.)

    # The "backwards" layer is the same as the first,
    # except that the backwards argument is set to True.
    l_lstm_back = lasagne.layers.recurrent.LSTMLayer(
        l_in, hiddenNum, ingate=gate_parameters,
        mask_input=l_mask, forgetgate=gate_parameters,
        cell=cell_parameters, outgate=gate_parameters, peepholes=False,
        learn_init=True, grad_clipping=100., backwards=True)

    l_concat = lasagne.layers.ConcatLayer([l_lstm, l_lstm_back], axis=2)

    return l_in, l_mask, l_concat


def genBiLSTMCrf(num_classes, featuresCount, hiddenNum):
    l_in, l_mask, l_concat = genBiLSTM(featuresCount, hiddenNum)

    n_batch, n_time_steps, n_features = l_in.input_var.shape
    l_crf = CRFLayer(l_concat, num_labels=num_classes, mask_input=l_mask)
    loss = objectives.crf_loss
    network_output = lasagne.layers.get_output(l_crf, deterministic=True)
    predict, _ = objectives.crf_accuracy(network_output, T.imatrix("NotUSed"))
    return l_in, l_mask, l_crf, loss, predict


def getBiLSTMDenseDropoutCrf(num_classes, featuresCount, hiddenNum):
    l_in, l_mask, l_concat = genBiLSTM(featuresCount, hiddenNum)
    l_dropout = lasagne.layers.DropoutLayer(l_concat, p=0.1)
    # l_dense = lasagne.layers.DenseLayer(l_dropout, num_classes, num_leading_axes=2, nonlinearity=lasagne.nonlinearities.softmax)
    l_dense = SoftmaxDenseLayer(l_dropout, num_classes, num_leading_axes=2)


    l_crf = CRFLayer(l_dense, num_labels=num_classes, mask_input=l_mask)
    loss = objectives.crf_loss
    network_output = lasagne.layers.get_output(l_crf, deterministic=True)
    predict, _ = objectives.crf_accuracy(network_output, T.imatrix("NotUSed"))
    return l_in, l_mask, l_crf, loss, predict


def getBiLSTMDenseCrf(num_classes, featuresCount, hiddenNum):
    l_in, l_mask, l_concat = genBiLSTM(featuresCount, hiddenNum)
    #l_dense = lasagne.layers.DenseLayer(l_concat, num_classes, num_leading_axes=2, nonlinearity=lasagne.nonlinearities.softmax)
    l_dense = SoftmaxDenseLayer(l_concat, num_classes, num_leading_axes=2)

    l_crf = CRFLayer(l_dense, num_labels=num_classes, mask_input=l_mask)
    loss = objectives.crf_loss
    network_output = lasagne.layers.get_output(l_crf, deterministic=True)
    predict, _ = objectives.crf_accuracy(network_output, T.imatrix("NotUSed"))
    return l_in, l_mask, l_crf, loss, predict


def getBiLSTMDense(num_classes, featuresCount, hiddenNum):
    l_in, l_mask, l_concat = genBiLSTM(featuresCount, hiddenNum)
    (n_batches, batch_legnth, _) = lasagne.layers.get_output_shape(l_concat)
    l_reshape = lasagne.layers.reshape(l_concat, (-1, hiddenNum * 2))
    l_dense = lasagne.layers.DenseLayer(l_reshape, num_classes, num_leading_axes=1, nonlinearity=lasagne.nonlinearities.softmax)
    l_out = l_dense
    network_output = lasagne.layers.get_output(l_out, deterministic=True)
    predict = T.argmax(network_output, axis=-1)
    predict = T.reshape(predict, T.shape(l_mask.input_var))

    def myloss(p, target, mask):
        return lasagne.objectives.categorical_crossentropy(T.reshape(p, (-1, num_classes)), T.flatten(target)) * T.flatten(mask) * 10000

    loss = myloss
    return l_in, l_mask, l_out, loss, predict


def getBiLSTMFullDense(num_classes, featuresCount, hiddenNum):
    l_in, l_mask, l_concat = genBiLSTM(featuresCount, hiddenNum)
    l_dense = lasagne.layers.DenseLayer(l_concat, num_classes, nonlinearity=lasagne.nonlinearities.sigmoid)
    network_output = lasagne.layers.get_output(l_dense, deterministic=True)
    predict = T.argmax(network_output, axis=2)

    def myloss(p, target, mask):
        return lasagne.objectives.categorical_crossentropy(T.reshape(p, (-1, num_classes)), T.flatten(target)) * T.flatten(mask)

    loss = myloss
    return l_in, l_mask, l_dense, loss, predict


def genOnlyBilstm(num_classes, featuresCount, hiddenNum):
    assert hiddenNum == num_classes
    l_in = lasagne.layers.InputLayer(shape=(None, None, featuresCount))
    l_mask = lasagne.layers.InputLayer(shape=(None, None))
    l_lstm = lasagne.layers.recurrent.LSTMLayer(
        l_in, hiddenNum,
        # We need to specify a separate input for masks
        mask_input=l_mask,
        # Here, we supply the gate parameters for each gate
        ingate=gate_parameters, forgetgate=gate_parameters,
        cell=cell_parameters, outgate=gate_parameters, peepholes=False,
        # We'll learn the initialization and use gradient clipping
        learn_init=True, grad_clipping=100.)

    # The "backwards" layer is the same as the first,
    # except that the backwards argument is set to True.
    l_lstm_back = lasagne.layers.recurrent.LSTMLayer(
        l_in, hiddenNum, ingate=gate_parameters,
        mask_input=l_mask, forgetgate=gate_parameters,
        cell=cell_parameters, outgate=gate_parameters, peepholes=False,
        learn_init=True, grad_clipping=100., backwards=True)

    l_sum = lasagne.layers.ElemwiseSumLayer([l_lstm, l_lstm_back])

    network_output = lasagne.layers.get_output(l_sum, deterministic=True)
    predict = T.argmax(network_output, axis=2)

    def myloss(p, target, mask):
        return lasagne.objectives.categorical_crossentropy(T.reshape(p, (-1, num_classes)), T.flatten(target)) * T.flatten(mask)  # * 10000

    return l_in, l_mask, l_sum, myloss, predict
