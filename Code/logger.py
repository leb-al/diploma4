from __future__ import print_function

import time


class FileLogger:
    def __init__(self, fname, screen):
        timeformat = '%d-%m-%Y--%H-%M-%S'
        self.file = open("logs/" + fname + time.strftime(timeformat) + ".txt", "w")
        self.screen = screen

    def printline(self, line, end='\n'):
        if self.screen:
            print(line, end=end)
        print(line, end=end, file=self.file)
        self.file.flush()


class PrintLogger:
    def __init__(self):
        return

    def printline(self, line, end='\n'):
        print(line, end=end)
