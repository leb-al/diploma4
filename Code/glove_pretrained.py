from __future__ import print_function
import numpy as np

dictionary = {}

dimen = 0

pretrained = open('glove.6B.100d.txt')
for line in pretrained:
    tokens = line.split(' ')
    word = tokens[0]
    tokens = tokens[1:]
    word = word.upper()
    converted = ''
    for c in word:
        if c.isalnum():
            converted += c
        else:
            if c.isdigit():
                converted += '1'
            else:
                converted += '_'
    vector = [float(i) for i in tokens]
    dictionary[converted] = vector
    if dimen == 0:
        dimen = len(vector)
    assert dimen == len(vector)


def representation(word):
    if dictionary.has_key(word):
        return dictionary[word]
    else:
        return np.zeros(dimen)

def vect_dimetions():
    return dimen
