from __future__ import print_function

import lasagne
import numpy as np

import Code.builder as builder
import Code.logger as logger
from Code.loader import StandartDataLoader
from Code.trainer import Trainer, OneNetTrainer


def testRun(nets, loader, batch_size, learning_rate, rate_scale, netNames, epochNums):
    _, _, _, _, datasets = loader.loadStandart("dataset/empty", ['test3'], logger.PrintLogger())
    print("Data converted")
    num_classes = loader.numClasses()
    features_count = loader.featuresCount()
    for k in range(len(netNames)):
        trainers = []
        for i in range(len(nets[k])):
            trainers.append(OneNetTrainer(nets[k][i], num_classes[i], None, learning_rate[i], rate_scale))
        worker = Trainer(trainers, features_count)
        worker.setData(None, None, None, None, [datasets, datasets])
        print("Starting test")
        netName = netNames[k]
        for j in range(len(nets[k])):
            print('New net')
            epochNum = epochNums[k][j]
            f = np.load("../newResults/{}/net{}epoch{}.npz".format(netName, j, epochNum))
            param_values = [f['arr_%d' % i] for i in range(len(f.files))]
            f.close()
            lasagne.layers.set_all_param_values(trainers[j].l_out, param_values)
            worker.testEpoch(epochNum, batch_size, datasets[0], j)


def testN2():
    loader = StandartDataLoader(False, False, True, False)
    testRun([[builder.getBiLSTMDenseCrf(5, loader.featuresCount(), 50),
              builder.getBiLSTMDenseCrf(14, loader.featuresCount(), 70)]], loader, 200, [0.005, 0.015], 0.9, ["N2"], [[11]])


def testN3N4():
    loader = StandartDataLoader(False, False, True, False)
    testRun([[builder.getBiLSTMDense(5, loader.featuresCount(), 50), builder.getBiLSTMDense(14, loader.featuresCount(), 50)],
             [builder.getBiLSTMDenseCrf(5, loader.featuresCount(), 50), builder.getBiLSTMDenseCrf(14, loader.featuresCount(), 50)]],
            loader, 200, [0.005, 0.015], 0.9, ["N3"], [[20, 22]])


def testN5():
    loader = StandartDataLoader(False, True, True, True)
    testRun([[builder.getBiLSTMDenseCrf(5, loader.featuresCount(), 75),
              builder.getBiLSTMDenseCrf(14, loader.featuresCount(), 75)]],
            loader, 100, [0.005, 0.001], 0.9, ["N5"], [[11, 19]])


def testN7():
    loader = StandartDataLoader(False, False, True, True)
    testRun([[builder.getBiLSTMDenseCrf(5, loader.featuresCount(), 50),
              builder.getBiLSTMDenseCrf(14, loader.featuresCount(), 50)]], loader, 120, [0.005, 0.001], 0.9, ["N7_1"], [[13, 1]])


testN7()
